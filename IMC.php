<?php

class Personne {

    // Création des variables pour calcul IMC

    public float $poids;
    public float $taille;

    // Initialisation des fonctions et des variables

    public function __construct(float $poids, float $taille)
    {
        $this->poids = $poids;
        $this->taille = $taille;
    }

    // On set le poids et l'âge avec des paramètres

    public function setPoids(float $paraPoids):void
    {
        $this->poids = $paraPoids;
    }

    public function settaille(float $paraTaille):void
    {
        $this->taille = $paraTaille;
    }

    // On récupère les infos de l'âge et du poids

    public function getPoids():float
    {
        return $this->poids;
    }

    public function getTaille():float
    {
        return $this->taille;
    }

    // On calcule l'IMC

    public function calculate() {
        return $this->poids / ($this->taille * $this->taille);
    }

    // Recupère le status de la personne

    public function getStatus() {
        $imc = $this->calculate();
        if($imc < 18.5) {
            return "Insuffisance pondérale (Maigreur)";
        } elseif($imc >= 18.5 && $imc < 25) {
            return "Poids normal";
        } else {
            return "Surpoids";
        }
    }

}

$calcul = new Personne(150.0, 187);
echo "Votre IMC est de : ". $calcul->calculate();
echo "Votre status de poids est : ". $calcul->getStatus();